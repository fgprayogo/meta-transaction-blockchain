// SPDX-License-Identifier: MIT
pragma solidity ^0.8;
import "./IERC20Permit.sol";
import "hardhat/console.sol";

contract Receiver {
    IERC20Permit public immutable token;

    struct EIP712Domain {
        string  name;
        string  version;
        uint256 chainId;
        address verifyingContract;
    }

    struct Permit {
        address owner;
        address spender;
        uint256 value;
        uint256 nonce;
        uint256 deadline;
        
    }

    bytes32 constant EIP712DOMAIN_TYPEHASH = keccak256(
        "EIP712Domain(string name,string version,uint256 chainId,address verifyingContract)"
    );

    bytes32 constant PERMIT_TYPEHASH = keccak256(
        "Permit(address owner,address spender,uint256 value,uint256 nonce,uint256 deadline)"
    );

    bytes32 DOMAIN_SEPARATOR;

    constructor (address _token, string memory _tokenName, uint256 _chainId) public {
        DOMAIN_SEPARATOR = hash(EIP712Domain({
            name: _tokenName,
            version: "1",
            chainId: _chainId,
            verifyingContract: _token
        }));
        token = IERC20Permit(_token);
    }

    function hash(EIP712Domain memory eip712Domain) internal pure returns (bytes32) {
        return keccak256(abi.encode(
            EIP712DOMAIN_TYPEHASH,
            keccak256(bytes(eip712Domain.name)),
            keccak256(bytes(eip712Domain.version)),
            eip712Domain.chainId,
            eip712Domain.verifyingContract
        ));
    }


    function hash(Permit memory _transferData) internal pure returns (bytes32) {
        return keccak256(abi.encode(
            PERMIT_TYPEHASH,
            _transferData.owner,
            _transferData.spender,
            _transferData.value,
            _transferData.nonce,
            _transferData.deadline
        ));
    }

    function verify(Permit memory _transferData, uint8 v, bytes32 r, bytes32 s) public view returns (bool) {
        bytes32 digest = keccak256(abi.encodePacked(
            "\x19\x01",
            DOMAIN_SEPARATOR,
            hash(_transferData)
        ));
        return ecrecover(digest, v, r, s) == _transferData.owner;
    }

    struct BatchTransferData {
        address owner;
        address spender;
        address to;
        uint256 value;
        uint256 nonce;
        uint256 deadline;
        uint8 v;
        bytes32 r;
        bytes32 s;
    }
    event Log(string trxType, bool status, string message);
    event LogBytes(string trxType, bool status, bytes data);

    function batchTransfer(BatchTransferData[] memory _batchTransferData) public {
        require(_batchTransferData.length > 0, "YOU NEED TO PASS AT LEAST 1 TRANSFER DATA");
        uint i;
        for (i = 0; i< _batchTransferData.length; i++){
            Permit memory data = Permit(
                _batchTransferData[i].owner,
                _batchTransferData[i].spender,
                _batchTransferData[i].value,
                _batchTransferData[i].nonce,                 
                _batchTransferData[i].deadline
            );
            if(verify(data, _batchTransferData[i].v, _batchTransferData[i].r, _batchTransferData[i].s)){
                console.log("Nonce", _batchTransferData[i].nonce);
                try token.permit(
                    _batchTransferData[i].owner, 
                    _batchTransferData[i].spender, 
                    _batchTransferData[i].value, 
                    _batchTransferData[i].deadline, 
                    _batchTransferData[i].v, 
                    _batchTransferData[i].r, 
                    _batchTransferData[i].s
                ){   

                    emit Log("PERMIT", true, "SUCCESSFULLY PERMIT THE DATA");
                    try token.transferFrom(
                        _batchTransferData[i].owner, 
                        _batchTransferData[i].to, 
                        _batchTransferData[i].value
                    ) {
                        emit Log("TRANSFERFROM", false, "OK");
                    }catch Error(string memory reason) {
                        emit Log("TRANSFERFROM", false, reason);
                    } catch (bytes memory reason) {
                        emit LogBytes("TRANSFERFROM", false, reason);
                    }

                } catch Error(string memory reason) {
                    emit Log("PERMIT", false, reason);
                } catch (bytes memory reason) {
                    emit LogBytes("PERMIT", false, reason);
                }
               
            }
        }
    }
    
}