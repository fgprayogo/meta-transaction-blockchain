import express, { Express, Request, Response } from 'express';
let dotenv = require('dotenv').config()
import { ethers } from 'ethers'
import { TypedDataUtils } from 'ethers-eip712'
import receiver from './smart_contract/artifacts/contracts/Receiver.sol/Receiver.json'
import sampleToken from './smart_contract/artifacts/contracts/SampleToken.sol/SampleToken.json'
const receiverAbi = receiver.abi
const sampleTokenAbi = sampleToken.abi
const receiverAddress: any = process.env.REACT_APP_RECEIVER_ADDRESS
const sampleTokenAddress: any = process.env.REACT_APP_SAMPLE_TOKEN_ADDRESS
const provider = new ethers.providers.JsonRpcProvider(process.env.RPC_URL)
const owner = new ethers.Wallet(`${process.env.DEPLOYER_PRIVATE_KEY}`, provider)
const sampleTokenContract = new ethers.Contract(sampleTokenAddress, sampleTokenAbi, owner)
const receiverContract = new ethers.Contract(receiverAddress, receiverAbi, owner)


const app: Express = express();

var cors = require('cors')
app.use(cors())
app.use(express.json());


let batchData: any = []

app.get('/', async (req: Request, res: Response) => {
    res.json({ batchData })
})

app.post('/submit-trx', async (req: Request, res: Response) => {
    let error = false;
    if (req.body.length !== 9) {
        return res.status(400).json({ message: "Invalid Arguments Length" })
    }
    for (let i = 0; i < 9; i++) {
        if (req.body[i] === '') {
            error = true
        }
    }
    if (error == false) {
        const val = {
            owner: req.body[0],
            spender: req.body[1],
            value: req.body[3],
            nonce: req.body[4],
            deadline: req.body[5],
        };
        if (await receiverContract.verify(val, req.body[6], req.body[7], req.body[8])) {
            await batchData.push(req.body)
            return res.status(200).json({ message: "Ok" })
        }
        return res.status(400).json({ message: "Invalid Signature" })
    } else {
        return res.status(400).json({ message: "Invalid Arguments" })
    }
})

async function sendMetaTrx() {
    setTimeout(sendMetaTrx, 60000);
    if (batchData.length > 0) {
        console.log("Sending batch transfer ...")
        await receiverContract.batchTransfer(batchData)
        batchData = []
    }
}

sendMetaTrx();

app.listen(4321, () => {
    console.log('Running on port 4321')
})
